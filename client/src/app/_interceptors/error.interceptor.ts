import { ToastrService } from 'ngx-toastr';
import { NavigationExtras, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  // על ידי יירוטה לפני שהבקשה נשלחת לשרת HTTP-המיירט עוזר לנו לשנות את בקשת ה
  // המיירט יכול להיות שימושי להוספת כותרות מותאמות אישית לבקשה היוצאת, רישום התגובה הנכנסת וכו'
  //כאן אני משתמשת בו כדי לתפוס את השגיאה
  constructor(private router: Router, private toastr: ToastrService) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError(error => {
        if (error)
        debugger
        switch (error.status) {
          case 400:
            if (error.error.errors) {
              const modelStateErrors = [];
              for (let err in error.error.errors)
                modelStateErrors.push(error.error.errors[err])
              throw modelStateErrors.flat();
            }
            else if(error?.error)
            this.toastr.error(error.error, error.status);

            else {
              this.toastr.error(`Bad Request`, error.status);
            }
            break;
          case 401:
            this.toastr.error("Unauthorized", error.status)
            break;
          case 404:
            this.router.navigateByUrl('/not-found');
            break;
          case 500:
            const navigationExtras: NavigationExtras = { state: { error: error.error } };
            this.router.navigateByUrl('/server-error', navigationExtras);
            break;
          default:
            this.toastr.error('Something unexpected want worng');
            console.log(error);
            break;
        }
        return throwError(error);
      })
    )
  }
}
