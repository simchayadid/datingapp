import { AccountService } from './../_services/account.service';
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../_models/user';
import { take } from 'rxjs/operators';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  constructor(private accountService:AccountService) {}
// כאן אני מוסיפה לכל בקשה את הטוקן כך שהוא מתווסף אוטומטית להדרס של כל בקשה
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let currentUser : User;
    // אני מוודא שאחרי נתון אחד שיגיע ההרשמה לאובזרוובול תיסגר לבד pipe(take(1) -ע"י שימוש ב
    this.accountService.currentUser$.pipe(take(1)).subscribe(user=> currentUser=user);
    if(currentUser) {
      request=request.clone({
        setHeaders:{
          Authorization: `Bearer ${currentUser.token}`
        }
      })
    }
    return next.handle(request);
  }
}
