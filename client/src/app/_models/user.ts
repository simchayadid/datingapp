export interface User{
    username: string;
    token: string;
    mainPhotoUrl: string
}