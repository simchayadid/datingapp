import { environment } from './../../environments/environment';
import { User } from './../_models/user';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  baseUrl = environment.apiUrl;
  //הוא סוג של אובזרוובול שמאכסן רק את מספר הערכים שבתוך הסוגריים ReplaySubject
  private currentUserSource = new ReplaySubject<User>(1);
  currentUser$ = this.currentUserSource.asObservable();

  constructor(private http: HttpClient) {
  }

  login(model: any) {
    // debugger
    return this.http.post(`${this.baseUrl}account/login`, model).pipe(
      map((response: User) => {
        const user = response;
        if (user) {
          this.setCurrentUser(user);
          this.currentUser$.subscribe(data =>
            console.log(data));
        }
      })
    )
  }

  register(model: any) {
    debugger
    return this.http.post(`${this.baseUrl}account/register`, model).pipe(
      map((user: User) => {
        if (user) {
          this.currentUserSource.next(user);
        }
        return user;
      })
    )
  }

  setCurrentUser(user: User) {
    localStorage.setItem('user', JSON.stringify(user));
    this.currentUserSource.next(user);
  }
  logout() {
    localStorage.removeItem('user');
    this.currentUserSource.next(null);
    console.log("yutuytutuyt");
    this.currentUser$.subscribe(data =>
      console.log(data));

  }
}
