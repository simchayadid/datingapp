import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Member } from '../_models/members';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class MembersService {
  baseUrl = environment.apiUrl
  members: Member[] = [];

  constructor(private http: HttpClient) { }

  getMembers() {
    // מחזיר אובזרוובול return of
    if (this.members.length > 0) return of(this.members);
    return this.http.get<Member[]>(`${this.baseUrl}users`).pipe(
      map(members => {
        this.members = members;
        return this.members;
      })
    )
  }

  getMember(username: string) {
    const member = this.members.find(m=>m.username===username);
    if(member!==undefined)return of(member);
    return this.http.get<Member>(`${this.baseUrl}users/${username}`);
  }

  updateMember(member: Member) {
    debugger
    return this.http.put(`${this.baseUrl}users`, member).pipe(
      map(()=>{
        const index= this.members.indexOf(member);
        this.members[index]=member;
      })
    )
  }

  SetMainPhoto(photoId: number) {
    debugger
    return this.http.put(`${this.baseUrl}users/set-main-photo/${photoId}`,{});
  }


  DeletePhoto(photoId: number) {
    debugger
    return this.http.delete(`${this.baseUrl}users/delete-photo/${photoId}`,{});
  }
}
