import { MemberEditComponent } from './../members/member-edit/member-edit.component';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
// סוג שומר שמונע ניתוב מהקומפוננטה החוצה בתנאי מסוים - CanDeactivate 
export class PreventUnsavedChangesGuard implements CanDeactivate<unknown> {
  canDeactivate(component: MemberEditComponent): boolean {
    
    if (component.editForm.dirty)
      return confirm('Are you sure you want continue? ');
    return true;
  }

}
