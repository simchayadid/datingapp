import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AccountService } from '../_services/account.service';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  //יוצר פרופרטי שיועבר מקומפוננטת הבן לקומפוננטת האב Output
  @Output() cancelRegister = new EventEmitter();
  //יוצר פרופרטי שיועבר מקומפוננטת האב לקומפוננטת הבן Input
  // @Input() usersFromHomeComp: any;
  // model: any = {};
  registerForm: FormGroup;
  maxDate:Date;
  validationErrors: string[] = [];

  constructor(public accountService: AccountService, private toastr: ToastrService,
    private fb: FormBuilder,private router:Router) { 
    }

  ngOnInit(): void {
    this.intitializeForm();
    this.maxDate=new Date();
    debugger
    this.maxDate.setFullYear(this.maxDate.getFullYear() -18);
  }

  intitializeForm() {
    // FormBuilder -שימוש ב
    //הגדרה של כל השדות בטופס, עם הערך הראשוני של כל שדה ומערך עם תנאי האימות 
    this.registerForm = this.fb.group({
      gender: ['male'],
      username: ['', [Validators.required]],
      knownAs: ['', [Validators.required]],
      dateOfBirth: ['', [Validators.required]],
      city: ['', [Validators.required]],
      country: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(8)]],
      confirmPassword: ['', [Validators.required,this.matchValues('password')]]
    })
    // this.registerForm = new FormGroup({
    //   username: new FormControl('', [Validators.required]),
    //   password: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(8)]),
    //   confirmPassword: new FormControl('', [Validators.required,this.matchValues('password')])
    // })
    //בגלל שמשמתמש יכול לערוך שינויים בסיסמא אחרי שהכניס את האימות, ואז זה נשאר תקין
    //אני מריצה את הבדיקות על שדה האימות בכל פעם שיש שינוי בשדה הסיסמא
    this.registerForm.controls.password.valueChanges.subscribe(()=>{
      this.registerForm.controls.confirmPassword.updateValueAndValidity();
    })
  }
// פונקציה שמקבלת control ובודקת האם הערך שלו שווה לערך של הקונטרול איליו אנחנו רוצים להשוות (matchTo) 
  matchValues(matchTo: string): ValidatorFn {
    debugger
    return (control: AbstractControl) => {
      return control?.value === control?.parent?.controls[matchTo].value
      ? null: {isMatching: true};
    }
  }

  

  register() {
    debugger
    this.accountService.register(this.registerForm.value)
      .subscribe(response => {
        this.router.navigateByUrl('/members');
      }
        , error => {
this.validationErrors=error;          
        }
      )
  }

  //לחיצה על הכפתור מפעילה פונקציה ששולחת נתונים לאבא
  //(123) emit -בקומפוננטת האבא מייצג את מה שנשלח ב $event -ה
  cancel() {
    this.cancelRegister.emit("123");
  }
}
