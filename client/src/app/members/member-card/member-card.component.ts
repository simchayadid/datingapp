import { Member } from './../../_models/members';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-member-card',
  templateUrl: './member-card.component.html',
  styleUrls: ['./member-card.component.css']
})
export class MemberCardComponent implements OnInit {
//בגלל שהתנותים יתקבלו מקומפוננטת האב Input הספת שדה 
  @Input() member : Member;


  constructor() { }

  ngOnInit(): void {
  }

}
