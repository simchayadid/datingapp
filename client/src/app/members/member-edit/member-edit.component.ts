import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { take } from 'rxjs/operators';
import { MembersService } from './../../_services/members.service';
import { AccountService } from './../../_services/account.service';
import { User } from './../../_models/user';
import { Member } from './../../_models/members';
import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { pipe } from 'rxjs';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-member-edit',
  templateUrl: './member-edit.component.html',
  styleUrls: ['./member-edit.component.css']
})
export class MemberEditComponent implements OnInit {
  member: Member;
  user: User;
  @ViewChild('editForm') editForm: NgForm;
  // מאזין שמאפשר לנו גישה לאירועי דפדפן גם מאנגולר- HostListener
  @HostListener('window:beforeunload', ['$event']) unloadNotification($event: any) {
    if (this.editForm.dirty) {
      $event.returnValue = true;
    }
  }
  constructor(private accountService: AccountService, private membersService: MembersService,
    private toastr: ToastrService) {
    this.accountService.currentUser$.pipe(take(1)).subscribe(user => this.user = user);
  }

  ngOnInit(): void {
    this.loadMember();
  }
  loadMember() {
    this.membersService.getMember(this.user.username).subscribe(
      member => this.member = member);
  }

  updateMember() {
    this.membersService.updateMember(this.member).subscribe(() => {

      // לאחר שהתונים עודכנו בהצלחה במסד
      this.toastr.success('Profile update successfully');
      // מאתחלת את הנתונים בטופס לנתונים לאחר שמירת השינויים
      this.editForm.reset(this.member);
    });
  }

 
}
