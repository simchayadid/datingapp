import { Photo } from './../../_models/Photo';
import { MembersService } from './../../_services/members.service';
import { take } from 'rxjs/operators';
import { AccountService } from './../../_services/account.service';
import { Component, OnInit, Input } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { Member } from 'src/app/_models/members';
import { User } from 'src/app/_models/user';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-photo-editor',
  templateUrl: './photo-editor.component.html',
  styleUrls: ['./photo-editor.component.css']
})
export class PhotoEditorComponent implements OnInit {
  @Input() member: Member;
  uploader: FileUploader;
  hasBaseDropzoneOver = false;
  baseUrl = environment.apiUrl;
  user: User;
  constructor(private accountService: AccountService, private membersService: MembersService) {
    this.accountService.currentUser$.pipe(take(1)).subscribe(user =>
      this.user = user
    );
  }

  ngOnInit(): void {
    this.initializeUploader();
  }

  fileOverBase(e: any) {
    this.hasBaseDropzoneOver = e;
  }

  initializeUploader() {
    // זו תצורת ההעלאה
    this.uploader = new FileUploader({
      // הכתובת שאילה שולחים את הקובץ
      url: `${this.baseUrl}users/add-photo`,
      //אני שולחת כאן את הטוקן http בגלל שאני לא שולחת בקשת שרת דרך
      authToken: `Bearer ${this.user.token}`,

      isHTML5: true,
      allowedFileType: ['image'],
      removeAfterUpload: true,
      //  האם אני רוצה שההעלאה תבוצע אוטומטית - לא, יש ללחוץ על כפתור
      autoUpload: false,
      maxFileSize: 10 * 1024 * 1024
    });
    debugger
    this.uploader.onAfterAddingAll = (file) => {
      // debugger
      //אני לא רוצה לשלוח אישורים עם הקובץ כי אני כבר שולחת את הטוקן
      file.withCredention = false;
    }
    this.uploader.onSuccessItem = (item, response) => {
      if (response) {
        const photo = JSON.parse(response);
        //אם זו התמונה הראשונה של המשתמש
        if (this.member.photos.length <= 0) {
          this.user.mainPhotoUrl = photo.url;
          // עדכון של המשתמש החדש בלוקאל סטורג
          this.accountService.setCurrentUser(this.user);
          //משנה את התמונה הראשית
          this.member.photoUrl = photo.url;
        }
        this.member.photos.push(photo);

      }
    }
  }

  setMainPhoto(photo: Photo) {
    debugger
    this.membersService.SetMainPhoto(photo.id).subscribe(() => {
      // אחרי הקריאה לשרת בה עודכנה התמונה הראשית במסד
      // עושים שינויים בסרוויסים באתר
      /////////////////////////////////////////////
      // מעדכנת את התמונה הראשית במשתמש
      this.user.mainPhotoUrl = photo.url;
      // עדכון של המשתמש החדש בלוקאל סטורג
      this.accountService.setCurrentUser(this.user);
      //משנה את התמונה הראשית
      this.member.photoUrl = photo.url;
      // מגדירה בגלריית העריכה מי הראשית
      this.member.photos.forEach(p => {
        if (p.isMain) p.isMain = false;
        if (p.id === photo.id) p.isMain = true;
      });
    });
  }

  deletePhoto(photo: Photo) {
    this.membersService.DeletePhoto(photo.id).subscribe(() => {
      var deletePhoto = this.member.photos.indexOf(photo);
      this.member.photos.splice(deletePhoto, 1);

    });
  }

}
