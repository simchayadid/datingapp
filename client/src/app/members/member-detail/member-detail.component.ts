import { MembersService } from './../../_services/members.service';
import { Member } from './../../_models/members';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxGalleryAnimation, NgxGalleryImage, NgxGalleryOptions } from '@kolkov/ngx-gallery';

@Component({
  selector: 'app-member-detail',
  templateUrl: './member-detail.component.html',
  styleUrls: ['./member-detail.component.css']
})
export class MemberDetailComponent implements OnInit {
  member: Member;
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  constructor(private membersService: MembersService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.loadMember();

    this.galleryOptions = [
      {
        width: '500px',
        height: '500px',
        imagePercent: 100,
        thumbnailsColumns: 4,
        imageAnimation: NgxGalleryAnimation.Slide,
        preview: false
      }
    ]

  }

  getImages(): NgxGalleryImage[] {
    const imageUrls = [];
    for (const photo of this.member.photos) {
      imageUrls.push({
        small: photo?.url,
        medium: photo?.url,
        big: photo?.url
      });
    }
    return imageUrls
  }
  //  url-פונקציה שקוראת לשרת ומחזירה משתמש לפי שם המשתמש שנשלח ב
  //פונקציה שמופעלת ברגע שנטענת הקומפוננטה
  loadMember() {

    // url -נותן גישה לפרמטרים שנשלחים ב route.snapshot.paramMap.get('username')
    this.membersService.getMember(this.route.snapshot.paramMap.get('username')).subscribe(member => {
      console.log(member);
      this.member = member;
      this.galleryImages = this.getImages();

    })
  }
}
