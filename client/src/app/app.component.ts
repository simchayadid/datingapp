import { User } from './_models/user';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AccountService } from './_services/account.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Dating app';

  constructor(private http: HttpClient, public accountService : AccountService) { 
    // console.log("ftghh")
    // accountService.currentUser$.subscribe(data=>
    //   console.log(data));
  }

  ngOnInit() {
    this.setCurrentUser();
  }
//פונקציה שמטפלת במצב שהמשתמש נכנס לאתר, לא עשה עדיין התחברות אבל יש טוקן בלוקאל סטורג
//הפונקציה לוקחת מה שיש בלוקאל סטורג' ושמה באובזרוובול
  setCurrentUser() {
    const user : User= JSON.parse(localStorage.getItem('user'));
    this.accountService.setCurrentUser(user);
  }
 
}
