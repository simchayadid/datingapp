import { Component, Input, OnInit, Self } from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';
//קומפוננטה שמייצגת שדה input שנוצר באופן דינמי   
@Component({
  selector: 'app-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.css']
})

export class TextInputComponent implements ControlValueAccessor {
  @Input() label: string;
  @Input() type = 'text';

  //כאן אני מוודאה שהקונטרול עצמו ישלח
  constructor(@Self() public ngControl: NgControl) {
    this.ngControl.valueAccessor = this;

  }
  writeValue(obj: any): void {

  }
  registerOnChange(fn: any): void {

  }
  registerOnTouched(fn: any): void {

  }




}
