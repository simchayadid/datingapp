import { PreventUnsavedChangesGuard } from './_guards/prevent-unsaved-changes.guard';
import { MemberEditComponent } from './members/member-edit/member-edit.component';
import { ServerErrorComponent } from './errors/server-error/server-error.component';
import { TestErrorsComponent } from './errors/test-errors/test-errors.component';
import { MessagesComponent } from './messages/messages.component';
import { ListsComponent } from './lists/lists.component';
import { MemberListComponent } from './members/member-list/member-list.component';
import { MemberDetailComponent } from './members/member-detail/member-detail.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './_guards/auth.guard';
import { NotFoundComponent } from './errors/not-found/not-found.component';
//מערך המכיל את המסלולים באתר - את הניתוב והקומפוננטה אליה הוא יוביל
const routes: Routes = [
  // כאן אני מגדירה את הניתוב ובנוסף מגדירה מתי יהיה אפשר לגשת
  //במקום לעשות אתזה עבור כל נתיב אני מגדירה את זה בשביל כולם
  // {path:"members", component:MemberListComponent, canActivate:[AuthGuard]},
  { path: "", component: HomeComponent },
  {
    path: "",
    // מגדיר מתי השומרים יפעלו
    runGuardsAndResolvers: 'always',
    // הוא פרופרטי המכיל מערך עם רשימת השומרים canActivate
    canActivate: [AuthGuard],
    children: [
      { path: "members", component: MemberListComponent},
      { path: "members/:username", component: MemberDetailComponent },
      // מציין סוג נוסף של שומר canDeactivate:[PreventUnsavedChangesGuard]
      {path: "member/edit",component:MemberEditComponent, canDeactivate:[PreventUnsavedChangesGuard]},
      { path: "lists", component: ListsComponent },
      { path: "messages", component: MessagesComponent }
    ]
  },
  { path: "errors", component: TestErrorsComponent },
  { path: "not-found", component: NotFoundComponent },
  { path: "server-error", component: ServerErrorComponent },


  { path: "**", component: NotFoundComponent, pathMatch: 'full' }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
