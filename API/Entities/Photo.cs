using System.ComponentModel.DataAnnotations.Schema;

namespace API.Entities
{
    //  ייצוג לטבלת התמונות, בגלל שאני לא הולכת לגשת אליה ישירות אלא רק לאוסף התמונות של משתמש DataContext -אני לא יוצרת ב
    // אבל אני כן רוצה שיישות הצילום תיקרא תמונות במסד הנתונים
    // אז מה שאני יעשה כדי להבטיח שזה מה שיהיה, לתת למחלקה תכונה של טבלה 
    // ואני יכולה לציין את תמונות כשם הטבלה
    [Table("Photos")]
    public class Photo
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public bool IsMain { get; set; }
        public string PublicId { get; set; }
        public AppUser AppUser { get; set; }
        public int AppUserId { get; set; }
        
    }
}