using System.Security.AccessControl;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Threading.Tasks;
using API.Data;
using API.Entities;
using Microsoft.AspNetCore.Mvc;
using API.DTOs;
using Microsoft.EntityFrameworkCore;
using API.Interfaces;
using AutoMapper;

namespace API.Controllers
{
    public class AccountController : BaseApiController
    {
        private readonly DataContext _context;
        private readonly ITokenService _tokenService;
        private readonly IMapper _mapper;
        public AccountController(DataContext context, ITokenService tokenService, IMapper mapper)
        {
            _mapper = mapper;
            _tokenService = tokenService;
            _context = context;
        }

        [HttpPost("register")]
        public async Task<ActionResult<UserDto>> Register(RegisterDto registerDto)
        {
            if (await UserExists(registerDto.Username)) return BadRequest("User is Exists");

            var user = _mapper.Map<AppUser>(registerDto);
            //בשורה הבאה אני מגדירה שכשהשימוש במחלקה ייסתיים המחלקה תסגר בצורה טובה
            //כשאני לא מכניסה כלום בקונסטרקטור הוא יוצר מופע חדש עם קי חדש
            using var hmac = new HMACSHA512();

            user.UserName = registerDto.Username.ToLower();
            user.passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(registerDto.Password));
            user.passwordSalt = hmac.Key;



            _context.Users.Add(user);
            await _context.SaveChangesAsync();

            return new UserDto
            {
                Username = user.UserName,
                Token = _tokenService.CreateToken(user),
                KnownAs = user.KnownAs
            };

        }
        [HttpPost("login")]

        public async Task<ActionResult<UserDto>> login(LoginDto loginDto)
        {
            //מוצאת את הראשון המתאים וזורקת שגיאה אם יש יותר מאחד SingleOrDefaultAsync הפונקציה 
            var user = await _context.Users
            .Include(p => p.Photos)
            .SingleOrDefaultAsync(u => u.UserName == loginDto.Username);

            if (user == null) return Unauthorized("Invalid UserName");
            //HMACSHA512 יצירה של מופע חדש למחלקה
            //עכשיו אני מכניסה לקונסטרקטור את המפתח של המשתמש שמצאתי בטבלה
            using var hmac = new HMACSHA512(user.passwordSalt);
            //המקורי Hash זה יהיה לפי המפתח שיצר את ה Hash לכן כשאני אצור 
            //המחושב שיצרתי כרגע לבין זה שמאוכסן בטבלה Hash מה שנותן לי את האפשרות להשוות בין ה
            var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(loginDto.Password));

            //הוא מערך אני עוברת על כל איבר Hash -בגלל שה
            //ואם אחד מהאיברים לא תואם אני מחזירה שגיאה
            for (int i = 0; i < computedHash.Length; i++)
            {
                if (computedHash[i] != user.passwordHash[i]) return Unauthorized("Invalid Password");
            }
            //אם הגעתי לפ זה אומר שהסיסמה נכונה ואני מחזירה את המשתמש
            return new UserDto
            {
                Username = user.UserName,
                Token = _tokenService.CreateToken(user),
                MainPhotoUrl = user.Photos.FirstOrDefault(photo => photo.IsMain)?.Url,
                KnownAs=user.KnownAs
            };
        }

        //פונקציה שבודקת האם המשתמש קיים במערכת
        private async Task<bool> UserExists(string Username)
        {
            return await _context.Users.AnyAsync(u => u.UserName == Username.ToLower());
        }

    }
}