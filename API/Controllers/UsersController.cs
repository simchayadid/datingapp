using System.Security.Claims;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Data;
using API.DTOs;
using API.Entities;
using API.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using API.Extensions;

namespace API.Controllers
{
    // גורמת למחלקה להיות מוגנת - לחייב הרשאה Authorize
    //Authorization מחייב שכשנשלחת קריאה יהיה בהדרס פרופרטי של
    [Authorize]
    public class UsersController : BaseApiController
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly IPhotoService _photoService;

        public UsersController(IUserRepository userRepository, IMapper mapper, IPhotoService photoService)
        {
            _photoService = photoService;
            _mapper = mapper;
            _userRepository = userRepository;
        }

        // [AllowAnonymous]//ללא הגנה על הפונקציה
        [HttpGet]
        async public Task<ActionResult<IEnumerable<MemberDto>>> GetUsers()
        {
            // רשימת המשתמשים
            var users = await _userRepository.getMembersAsync();
            // מיפוי רשימת המשתמשים לרשימת חברים
            return Ok(users);
        }
        // גורמת לפונקציה להיות מוגנת - לחייב הרשאה Authorize
        //Authorization מחייב שכשנשלחת קריאה יהיה בהדרס פרופרטי של
        //אני מוסיפה מאפיין של שם כשי שאני אוכל לגשת לניתוב הזה ממקומות אחרים( מהפונקציה שמעלה תמונה)
        [HttpGet("{userName}", Name = "GetUser")]
        async public Task<ActionResult<MemberDto>> GetUser(string userName)
        {
            //    GetMemberAsync במקום לקבל משתמש ולמפות אותו לחבר, אני קוראת לפונקציה
            //   שממפה את הנתונים ממשתמש לחבר כשמבקשת את הנתונים מהטבלה 
            // var user = await _userRepository.GetUserByUserNameAsync(userName);
            // return (_mapper.Map<MemberDto>(user));
            
            return await _userRepository.GetMemberAsync(userName);
        }

        [HttpPut]
        public async Task<ActionResult> updateUser(MemberUpdateDto memberUpdateDto)
        {
            // כאן מתבצע חילוץ של שם המשתמש שמוצפן בתוך התוקן שנשלח בהדרס
            // שמוצפן בתוך התוקן Claims -הוא אחד מה nameid
            var user = await _userRepository.GetUserByUserNameAsync(User.GetUsername());
            //:כך נחסך ממני להציב כל נתון בפני עצמו לדוג ,user -ל memberUpdateDto -מיפוי כל הנתונים מ
            //user.City = memberUpdateDto.City;
            _mapper.Map(memberUpdateDto, user);

            // סימון המשצמש כישות שהשתנתה
            _userRepository.Update(user);
            //אם שמירת השינויים הצליחה לא מזירה כלום 
            if (await _userRepository.SaveAllAsync()) return NoContent();
            //אם נכשלה מחזירה בקשה שגויה
            return BadRequest("Failed to update the user");
        }

        [HttpPost("add-photo")]

        public async Task<ActionResult<PhotoDto>> AddPhoto(IFormFile file)
        {
            //קבלת המשתמש ע"י חילוץ השם שלו מהטוקן
            var user = await _userRepository.GetUserByUserNameAsync(User.GetUsername());
            //בקשה לשרת להעלאת הקובץ- תמונה
            var result = await _photoService.AddPhotoAsync(file);
            //במקרה שחזרו שגיאות
            if (result.Error != null) return BadRequest(result.Error.Message);
            //יצירת תמונה חדשה והצבת הנתונים שהתקבלו
            var photo = new Photo
            {
                Url = result.SecureUrl.AbsoluteUri,
                PublicId = result.PublicId
            };
            // הגדרת התמונה כראשית אם למשתמש אין תמונות
            if (user.Photos.Count == 0)
            {
                photo.IsMain = true;
            }
            //הוספת התמונה לתמונות
            user.Photos.Add(photo);
            //אם השינויים נשמרו בהצלחה
            if (await _userRepository.SaveAllAsync())
            {
                //אני מחזירה סטטוס של יצירת פרוייקטת ושולחת את הניתוב מאיפה אפשר לקבל את התמונה
                // ובגלל שהניתוב הזה מקבל את שם המשתמש אני שולחת אובייקט עם שם המשתמש
                // התשובה שחוזרת ללקוח: סטטוס 201 ובהדרס תחת מיקום נרשם איפה אפשר למצוא את התמונה
                return CreatedAtRoute("GetUser", new { username = user.UserName }, _mapper.Map<PhotoDto>(photo));

            }

            return BadRequest("Problem adding photo");

        }
        [HttpDelete("delete-photo/{photoId}")]

        public async Task<ActionResult> DeletePhoto(int photoId)
        {
            //קבלת המשתמש ע"י חילוץ השם שלו מהטוקן
            var user = await _userRepository.GetUserByUserNameAsync(User.GetUsername());
            var photo = user.Photos.FirstOrDefault(photo => photo.Id == photoId);

            if (photo == null) return NotFound();

            if (photo.IsMain) return BadRequest("You cannot delete your main photo");

            if (photo.PublicId != null)
            {
                var result = await _photoService.DeletePhotoAsync(photo.PublicId);
                if (result.Error != null) return BadRequest(result.Error.Message);
            }

            user.Photos.Remove(photo);

            if(await _userRepository.SaveAllAsync()) return Ok();

            return BadRequest("Failed to delete the photo");
        }

        [HttpPut("set-main-photo/{photoId}")]
        public async Task<ActionResult> SetMainFoto(int photoId)
        {
            var user = await _userRepository.GetUserByUserNameAsync(User.GetUsername());
            var photo = user.Photos.First(photo => photo.Id == photoId);

            if (photo.IsMain) return BadRequest("This is already your main photo");

            var currentMain = user.Photos.First(photo => photo.IsMain);
            currentMain.IsMain = false;
            photo.IsMain = true;

            if (await _userRepository.SaveAllAsync()) return NoContent();

            return BadRequest("Failed to set main photo");

        }
    }
}

// 