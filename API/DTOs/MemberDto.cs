using System;
using System.Collections.Generic;

namespace API.DTOs
{
    //   הזה כדי לשלוט על מה שאני מחזירה ללקוח DTO - אני יוצרת את ה
    // אני לא רוצה להחזיר לו את הסיסמה למשל
    // ןגם כדי לפתור את הבעיה של מעגל לא נגמר בהחזרת מערך התמונות
    public class MemberDto
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string PhotoUrl { get; set; }

    //   AppUser  יודע למפות את השדה הזה אוטומטית מפונקציית הגט שנמצאת במחלקת AutoMapper
        public int Age { get; set; }
        public string KnownAs { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastActive { get; set; }
        public string Gender { get; set; }
        public string Introduction { get; set; }
        public string LookingFor { get; set; }
        public string Interests { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public ICollection<PhotoDto> Photos { get; set; }

    }

    
}