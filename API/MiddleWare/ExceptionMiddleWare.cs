using System.Net;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using API.Errors;
using System.Text.Json;

namespace API.MiddleWare
{
    public class ExceptionMiddleWare
    {
        //מי המידלווור הבא שיופיע בצינור
        private readonly RequestDelegate _next;
        //אפשרות לכתוב הודעות לקונסול
        private readonly ILogger<ExceptionMiddleWare> _logger;
        //אפשרות לבדוק באיזה סביבה אנחנו רצים האם בייצור או בפיתוח
        private readonly IHostEnvironment _env;
        public ExceptionMiddleWare(RequestDelegate next, ILogger<ExceptionMiddleWare> logger, IHostEnvironment env)
        {
            _env = env;
            _logger = logger;
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                //הכנסה לצינור של בקשת השרת
                await _next(context);
            }
            catch (Exception ex)
            {
                //כתיבה של השגיאה לטרמינל
                _logger.LogError(ex, ex.Message);
                //כתיבה של השגיאה לתגובה- הגדרה שיהיה מסוג גייסון
                context.Response.ContentType = "application/json";
                //הגדרה של הסטטוס ל-500
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

                //הגדרה של התגובה לשני המצבים פיתוח וייצור
                var response = _env.IsDevelopment() ?
                new ApiException(context.Response.StatusCode, ex.Message, ex.StackTrace?.ToString()) :
                new ApiException(context.Response.StatusCode, "Internal Server Error");


                //הגדרה של תבנית הגייסון שתהיה בצורת כאמל קייס
                var options = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
                //יצירת סדרה של גייסון שמכילה את התגובה שייצרנו לפי האופציות שיצרנו
                var json = JsonSerializer.Serialize(response, options);
                //כתיבת התגובה
                await context.Response.WriteAsync(json);



            }
        }
    }
}