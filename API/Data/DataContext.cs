using API.Entities;
using Microsoft.EntityFrameworkCore;

namespace API.Data
{
    //מייצגת את מסד הנתונים DataContext המחלקה
    public class DataContext : DbContext

    {
        public DataContext(DbContextOptions options) : base(options)
        {
        }
        //מייצג את טבלת המשתמשים Users השדה 
        public DbSet<AppUser> Users { get; set; }

    }
}