using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using API.Entities;
using API.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace API.Services
{
    public class TokenService : ITokenService
    {
        private readonly SymmetricSecurityKey _key;
        public TokenService(IConfiguration config)
        {
            //appsettings -המפתח הוא קידוד של מפתח הטוקן שנמצא ב
            _key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["TokenKey"]));
        }

        public string CreateToken(AppUser user)
        {
            //הגדרה של רשימת טענות
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.NameId, user.UserName)
                //כמובן שאפשר להוסיף טענה נוספת
                // new Claim(JwtRegisteredClaimNames.Acr,"jhkjhkh")

            };

            var creds = new SigningCredentials(_key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                //'הוספת הטענות, כמו שם משתמש וכדו
                Subject = new ClaimsIdentity(claims),
                //תאריך התפוגה של הטוקן
                Expires = DateTime.Now.AddDays(7),
                //חתימת אבטחה
                SigningCredentials = creds

            };

            var tokenHandler = new JwtSecurityTokenHandler();
            //יצירת התוקן
            var token = tokenHandler.CreateToken(tokenDescriptor);
            //מחזיר את הטוקן עצמו
            return tokenHandler.WriteToken(token);

        }
    }
}