using System;
namespace API.Extensions
{
    //    ומוסיפה לה את הפונקציה לחישוב גיל DateTime מחלקה שמרחיבה את מחלקת
    public static class DateTimeExtensions
    {
        // 'דייט אוף בירת - dob
        public static int CalculateAge(this DateTime dob)
        {
            var today = DateTime.Today;
            var age = today.Year - dob.Year;
            // אם עדיין לא היה יום ההולדת מורידים שנה מהגיל
            if (dob.Date > today.AddYears(-age)) age--;
            return age;
        }
    }
}