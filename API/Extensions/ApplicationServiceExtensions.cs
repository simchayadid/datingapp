using API.Data;
using API.Helpers;
using API.Interfaces;
using API.Services;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace API.Extensions
{
    public static class ApplicationServiceExtensions
    {
        // IServiceCollection היא הרחבה למחלקה AddApplicationServices הפונקציה 
        public static IServiceCollection AddApplicationServices(this IServiceCollection services, IConfiguration config)
        {
            //הוספת האופציה להעלאת תמונות לשרת
            services.Configure<CloudinarySettings>(config.GetSection("CloudinarySettings"));
            //באינטרפייס אני שמה את כל החתימות של הפונקציות שאני רוצה שהסרוויס יממש
            //הסרוויס יממש את הפונקציות
            //אבל מהקונטרולר אני קוראת לפונקציות דרך מופע של האינטרפייס ולא של הסרוויס
            //אני מציינת שבכל פעם שמשתמשים באינטרפיים הוא יממש את מה שכתוב בסרוויס Startup וכדי שהכל יעבוד ב
            // :בפקודה הזו Startup וזה קורה ב
            // services.AddScoped<ITokenService, TokenService>();
            //בשורה הבאה אני אומרת לו שכל פעם שהקונטרולר רוצה להזריק את האינארפייס הוא צריך להשתמש בסרוויס 
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IPhotoService, PhotoService>();
            // הוספת המיפוי האוטומטי, כדי שנוכל להזריק אותו כתלות
            // AutoMapperProfiles וזה מספיק עבור אוטומאפר בשביל למצוא את 
            services.AddAutoMapper(typeof(AutoMapperProfiles).Assembly);

            //הגדרת החיבור למסד
            //בתוך הסוגריים המשולשות מציינים את שם המחלקה שמייצגת את הטבלה
            //בתוך ביטוי הלמדה מעבירים פרמטרים לפונקציה
            //הפרמטר הוא סוג המסד שבו משתמשים ובסוגריים הקונקשיין סטרינג
            services.AddDbContext<DataContext>(options =>
            {
                options.UseSqlite(config.GetConnectionString("DefaultConection"));
            });

            return services;

        }
    }
}