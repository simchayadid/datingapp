using API.Entities;

namespace API.Interfaces
{
    //אילו פונקציות הסרוויס טוקן חייב לממש
    public interface ITokenService
    {
         string CreateToken(AppUser user);
         
    }
}