using System.Collections.Generic;
using System.Threading.Tasks;
using API.DTOs;
using API.Entities;

namespace API.Interfaces
{
    public interface IUserRepository
    {
        void Update(AppUser user);
        Task<bool> SaveAllAsync();
        // הוא סוג של אוסף שניתן לעשות עליו כל מיני מעברים IEnumerable
        Task<IEnumerable<AppUser>> GetUsersAsync();

        Task<AppUser> GetUserByIdAsync(int id);
        Task<AppUser> GetUserByUserNameAsync(string usernamr);
        Task <IEnumerable<MemberDto>> getMembersAsync();

        Task<MemberDto> GetMemberAsync(string username);

    }
}