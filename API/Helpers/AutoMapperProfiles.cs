using System;
using System.Linq;
using API.DTOs;
using API.Entities;
using API.Extensions;
using AutoMapper;

namespace API.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        // הרעיון של מאפר הוא לעזור לנו למפות מאובייקט אחד למשנהו
        // לוקח את כל המאפיינים שנמצאים בשתי המחלקות ויש להם שם זהה AutoMapper
        public AutoMapperProfiles()
        {
            // מה שאנו מציינים כאן הוא מאיפה אנו רוצים למפות ולאן אנו רוצים למפות
            // אנחנו צריכים לציין מאיפה הוא מגיע photoUrl לא קיים המאפיין AppUser -בגלל שב 
            CreateMap<AppUser, MemberDto>()
            .ForMember(dest => dest.PhotoUrl, opt => opt.MapFrom(src =>
                    src.Photos.FirstOrDefault(x => x.IsMain).Url))
             .ForMember(dest =>dest.Age,opt=>opt.MapFrom(src=>
             src.DateOfBirth.CalculateAge()));
            
            CreateMap<Photo, PhotoDto>();
            CreateMap<MemberUpdateDto, AppUser>();
            CreateMap<RegisterDto, AppUser>();


        }
    }
}